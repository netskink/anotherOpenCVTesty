//
//  ViewController.h
//  anotherOpenCVTesty
//
//  Created by John Fred Davis on 5/6/16.
//  Copyright © 2016 Netskink Computing. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <opencv2/videoio/cap_ios.h>
using namespace cv;

@interface AnotherViewController : UIViewController<CvVideoCameraDelegate> {
    CvVideoCamera *videoCamera;

}
@property (nonatomic, retain) CvVideoCamera *videoCamera;
@property (nonatomic, retain) NSNumber *mode;
@property (nonatomic, retain) NSNumber *cameraState;

- (void)processImage:(Mat&)image;


@end

