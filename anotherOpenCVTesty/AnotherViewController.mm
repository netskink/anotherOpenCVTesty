//
//  ViewController.m
//  anotherOpenCVTesty
//
//  Created by John Fred Davis on 5/6/16.
//  Copyright © 2016 Netskink Computing. All rights reserved.
//
// This is the second tutorial.  Trying to build opencv from source failed.  I had to use
// the prebuilt binary from here:
//  http://opencv.org/downloads.html

#import "AnotherViewController.h"
#import <opencv2/opencv.hpp>

enum procssing_mode { eNORMAL=0, eGRAY, eINVERSE, eEDGE };



@interface AnotherViewController ()
@property (strong, nonatomic) IBOutlet UIImageView *imageView;
@property (strong, nonatomic) IBOutlet UIButton *startButton;

@end

@implementation AnotherViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    self.videoCamera = [[CvVideoCamera alloc] initWithParentView:self.imageView];
    self.videoCamera.defaultAVCaptureDevicePosition = AVCaptureDevicePositionFront;
    self.videoCamera.defaultAVCaptureSessionPreset = AVCaptureSessionPreset352x288;
    self.videoCamera.defaultAVCaptureVideoOrientation = AVCaptureVideoOrientationPortrait;
    self.videoCamera.defaultFPS = 30;
    self.videoCamera.grayscaleMode = NO;
    
    self.videoCamera.delegate = self;
    
    // Image processing mode
    self.mode = [NSNumber numberWithInt:eNORMAL];
    
    // camera on or off state
    self.cameraState = [NSNumber numberWithBool:false];
    
    
}

- (void)processImage:(Mat&)image {
    
    //
    // On normal, dont do anything.
    //
    if ([self.mode integerValue] == eNORMAL) {
        return;
    }
    
    //
    // Any processing modes, work with a copy
    //
    // Do some OpenCV stuff with the image
    cv::Mat image_copy;
    cvtColor(image, image_copy, COLOR_BGR2GRAY);
    
    // What mode?
    if ([self.mode integerValue] == eGRAY) {
        
        // for gray scale, we have already gotten it
        
    } else if ([self.mode integerValue] == eINVERSE) {
        
        // For inverted, we just bitwise not the gray scale
        bitwise_not(image_copy, image_copy);
        
    } else if ([self.mode integerValue] == eEDGE) {
        
        // For edge detection do gauss and canny
        GaussianBlur(image_copy, image_copy, cv::Size(7,7), 1.5, 1.5);
        Canny(image_copy, image_copy, 0, 30, 3);
        
    } else {
        
        // Unknown
        NSLog(@"Unknown filter");
    }
    
    
    
    
    
    //Convert BGR to BGRA (three channel to four channel)
    cv::Mat bgr;
    cvtColor(image_copy, bgr, COLOR_GRAY2BGR);

    cvtColor(bgr, image, COLOR_BGR2BGRA);
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



// Convert from UIImage to cv::Mat
// This code is used verbatim in the testyOpenCV demo
- (cv::Mat)cvMatFromUIImage:(UIImage *)image {
    
    CGColorSpaceRef colorSpace = CGImageGetColorSpace(image.CGImage);
    CGFloat cols = image.size.width;
    CGFloat rows = image.size.height;
    
    cv::Mat cvMat(rows, cols, CV_8UC4); // 8 bits per component, 4 channels (color channels + alpha)
    
    CGContextRef contextRef = CGBitmapContextCreate(cvMat.data,                 // Pointer to  data
                                                    cols,                       // Width of bitmap
                                                    rows,                       // Height of bitmap
                                                    8,                          // Bits per component
                                                    cvMat.step[0],              // Bytes per row
                                                    colorSpace,                 // Colorspace
                                                    kCGImageAlphaNoneSkipLast |
                                                    kCGBitmapByteOrderDefault); // Bitmap info flags
    
    CGContextDrawImage(contextRef, CGRectMake(0, 0, cols, rows), image.CGImage);
    CGContextRelease(contextRef);
    
    return cvMat;
}

// Convert from UIImage to cv::Mat (Gray?)
// This code was not used.  It appears to be for images with an alpha layer.
- (cv::Mat)cvMatGrayFromUIImage:(UIImage *)image {
    
    CGColorSpaceRef colorSpace = CGImageGetColorSpace(image.CGImage);
    CGFloat cols = image.size.width;
    CGFloat rows = image.size.height;
    
    // From above, this is the only difference
    cv::Mat cvMat(rows, cols, CV_8UC1); // 8 bits per component, 1 channels
    
    CGContextRef contextRef = CGBitmapContextCreate(cvMat.data,                 // Pointer to data
                                                    cols,                       // Width of bitmap
                                                    rows,                       // Height of bitmap
                                                    8,                          // Bits per component
                                                    cvMat.step[0],              // Bytes per row
                                                    colorSpace,                 // Colorspace
                                                    kCGImageAlphaNoneSkipLast |
                                                    kCGBitmapByteOrderDefault); // Bitmap info flags
    
    CGContextDrawImage(contextRef, CGRectMake(0, 0, cols, rows), image.CGImage);
    CGContextRelease(contextRef);
    
    return cvMat;
}


// Convert from cv::Mat to UIImage
// This code is used verbatim in the testyOpenCV demo
-(UIImage *)UIImageFromCVMat:(cv::Mat)cvMat {
    
    NSData *data = [NSData dataWithBytes:cvMat.data length:cvMat.elemSize()*cvMat.total()];
    CGColorSpaceRef colorSpace;
    
    if (cvMat.elemSize() == 1) {
        colorSpace = CGColorSpaceCreateDeviceGray();
    } else {
        colorSpace = CGColorSpaceCreateDeviceRGB();
    }
    
    CGDataProviderRef provider = CGDataProviderCreateWithCFData((__bridge CFDataRef)data);
    
    // Creating CGImage from cv::Mat
    CGImageRef imageRef = CGImageCreate(cvMat.cols,                                 //width
                                        cvMat.rows,                                 //height
                                        8,                                          //bits per component
                                        8 * cvMat.elemSize(),                       //bits per pixel
                                        cvMat.step[0],                            //bytesPerRow
                                        colorSpace,                                 //colorspace
                                        kCGImageAlphaNone|kCGBitmapByteOrderDefault,// bitmap info
                                        provider,                                   //CGDataProviderRef
                                        NULL,                                       //decode
                                        false,                                      //should interpolate
                                        kCGRenderingIntentDefault                   //intent
                                        );
    
    
    // Getting UIImage from CGImage
    UIImage *finalImage = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    CGDataProviderRelease(provider);
    CGColorSpaceRelease(colorSpace);
    
    return finalImage;
}
- (IBAction)actionStart:(id)sender {
    
    if ([self.cameraState boolValue]) {
        [self.videoCamera stop];
        self.cameraState = [NSNumber numberWithBool:false];
        [self.startButton setTitle:@"Start" forState:UIControlStateNormal];
    } else {
        [self.videoCamera start];
        self.cameraState = [NSNumber numberWithBool:true];
        [self.startButton setTitle:@"Stop" forState:UIControlStateNormal];
    }
    
}

- (IBAction)actionMode:(UISegmentedControl *)sender {
    
    if (sender.selectedSegmentIndex == eNORMAL) {
        NSLog(@"Normal");
    } else if (sender.selectedSegmentIndex == eGRAY) {
        NSLog(@"Gray");
            } else if (sender.selectedSegmentIndex == eINVERSE) {
        NSLog(@"Inverted");
    } else if (sender.selectedSegmentIndex == eINVERSE) {
        NSLog(@"Edge");
    } else {
        NSLog(@"Unknown");
    }
    self.mode = [NSNumber numberWithLong:sender.selectedSegmentIndex];

}




@end
